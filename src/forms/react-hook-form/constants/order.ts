export const bmwBrand = "bmw";
export const bmwModels = [
  "1series",
  "2series",
  "3series",
  "4series",
  "5series",
] as const;

export const audiBrand = "audi";
export const audiModels = ["a3", "a4", "a5", "a6", "a7"] as const;

export const toyotaBrand = "toyota";
export const toyotaModels = ["gt86", "supra"] as const;

export const brands = [bmwBrand, audiBrand, toyotaBrand] as const;

export const models = [...bmwModels, ...audiModels, ...toyotaModels] as const;

export const brandsWhichCanMadeInGermany = [bmwBrand, audiBrand] as const;

export const currencies = ["usd", "eur"] as const;

export const currenciesConverterMap = {
  USD_TO_EUR: 1.1,
  EUR_TO_USD: 0.9,
};
