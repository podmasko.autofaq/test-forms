import React, { useEffect, useState } from "react";
import {
  audiBrand,
  audiModels,
  bmwBrand,
  bmwModels,
  brands,
  brandsWhichCanMadeInGermany,
  currencies,
  currenciesConverterMap,
  toyotaBrand,
  toyotaModels,
} from "src/forms/react-hook-form/constants/order";
import {
  Control,
  UseFieldArrayRemove,
  UseFormGetValues,
  UseFormRegister,
  UseFormResetField,
  UseFormSetValue,
  UseFormWatch,
  useWatch,
} from "react-hook-form";
import { TOrder } from "src/forms/react-hook-form/types/order";

const currenciesOptions = currencies.map((currency) => ({
  label: currency.toUpperCase(),
  value: currency,
}));

const brandsOptions = brands.map((brand) => ({
  label: brand.toUpperCase(),
  value: brand,
}));

const bmwModelOptions = bmwModels.map((model) => ({
  label: `${model[0]} ${model.slice(1)}`,
  value: model,
}));

const audiModelOptions = audiModels.map((model) => ({
  label: model.toUpperCase(),
  value: model,
}));

const toyotaModelOptions = [
  {
    label: "GT86",
    value: toyotaModels[0],
  },
  {
    label: "Supra",
    value: toyotaModels[1],
  },
];

const brandsToModelsMap = {
  [bmwBrand]: bmwModelOptions,
  [audiBrand]: audiModelOptions,
  [toyotaBrand]: toyotaModelOptions,
};

export type TOrderFormValues = {
  orders: TOrder[];
};

type TTab = "main" | "price";

type TOrderProps = {
  register: UseFormRegister<TOrderFormValues>;
  control: Control<TOrderFormValues>;
  remove: UseFieldArrayRemove;
  index: number;
  resetField: UseFormResetField<TOrderFormValues>;
  setValue: UseFormSetValue<TOrderFormValues>;
  getValues: UseFormGetValues<TOrderFormValues>;
};

const usePrice = ({
  control,
  index,
  getValues,
  setValue,
}: Pick<TOrderProps, "control" | "index" | "getValues" | "setValue">) => {
  const priceFromWatch = useWatch<TOrderFormValues>({
    control: control,
    name: `orders.${index}.price.from`,
  }) as string; // TODO: убрать кастование

  const priceToWatch = useWatch<TOrderFormValues>({
    control: control,
    name: `orders.${index}.price.to`,
  }) as string; // TODO: убрать кастование

  const priceCurrencyWatch = useWatch<TOrderFormValues>({
    control: control,
    name: `orders.${index}.price.currency`,
  }) as TOrder["price"]["currency"]; // TODO: убрать кастование

  useEffect(() => {
    if (priceCurrencyWatch === "usd") {
      setValue(
        `orders.${index}.price.from`,
        Math.round(
          getValues().orders[index].price.from *
            currenciesConverterMap.EUR_TO_USD *
            1e1
        ) / 1e1
      );
      setValue(
        `orders.${index}.price.to`,
        Math.round(
          getValues().orders[index].price.to *
            currenciesConverterMap.EUR_TO_USD *
            1e1
        ) / 1e1
      );
    }

    if (priceCurrencyWatch === "eur") {
      setValue(
        `orders.${index}.price.from`,
        Math.round(
          getValues().orders[index].price.from *
            currenciesConverterMap.USD_TO_EUR *
            1e1
        ) / 1e1
      );
      setValue(
        `orders.${index}.price.to`,
        Math.round(
          getValues().orders[index].price.to *
            currenciesConverterMap.USD_TO_EUR *
            1e1
        ) / 1e1
      );
    }
  }, [priceCurrencyWatch, getValues, index, setValue]);

  useEffect(() => {
    const parsedPriceFromWatch = parseInt(priceFromWatch);

    if (
      priceFromWatch.length &&
      parsedPriceFromWatch > getValues().orders[index].price.to
    ) {
      setValue(`orders.${index}.price.to`, parsedPriceFromWatch);
    }
  }, [priceFromWatch, getValues, index, setValue]);

  useEffect(() => {
    const parsedPriceToWatch = parseInt(priceToWatch);

    if (
      priceToWatch.length &&
      parsedPriceToWatch < getValues().orders[index].price.from
    ) {
      setValue(`orders.${index}.price.from`, parsedPriceToWatch);
    }
  }, [priceToWatch, getValues, index, setValue]);
};

const useMadeInGermany = ({
  control,
  resetField,
  index,
}: Pick<TOrderProps, "control" | "resetField" | "index">) => {
  const brandWatch = useWatch<TOrderFormValues>({
    control: control,
    name: `orders.${index}.brand`,
  }) as TOrder["brand"]; // TODO: убрать кастование

  const canBeMadeInGermany = brandsWhichCanMadeInGermany.some(
    (brand) => brand === brandWatch
  );

  useEffect(() => {
    if (!canBeMadeInGermany) {
      resetField(`orders.${index}.madeInGermany`);
    }
  }, [canBeMadeInGermany, resetField, index]);

  return { canBeMadeInGermany };
};

const Order = (props: TOrderProps) => {
  const { register, resetField, index, control, remove, setValue, getValues } =
    props;
  const [activeTab, setActiveTab] = useState<TTab>("main");
  usePrice({ control, index, setValue, getValues });
  const { canBeMadeInGermany } = useMadeInGermany({
    control,
    resetField,
    index,
  });
  const brandWatch = useWatch<TOrderFormValues>({
    control: control,
    name: `orders.${index}.brand`,
  }) as TOrder["brand"]; // TODO: убрать кастование

  return (
    <div>
      <div>
        <button type="button" onClick={() => setActiveTab("main")}>
          main
        </button>
        <button type="button" onClick={() => setActiveTab("price")}>
          price
        </button>
      </div>
      {activeTab === "main" && (
        <div>
          <p>
            <label>brand: </label>
            <select {...register(`orders.${index}.brand`)}>
              {brandsOptions.map((brand) => {
                return (
                  <option key={brand.value} value={brand.value}>
                    {brand.label}
                  </option>
                );
              })}
            </select>
          </p>
          <p>
            <label>model: </label>
            <select {...register(`orders.${index}.model`)}>
              {brandsToModelsMap[brandWatch].map((model) => {
                return (
                  <option key={model.value} value={model.value}>
                    {model.label}
                  </option>
                );
              })}
            </select>
          </p>
          {canBeMadeInGermany && (
            <p>
              <label>made in germany: </label>
              <input
                {...register(`orders.${index}.madeInGermany`)}
                type="checkbox"
              />
            </p>
          )}
        </div>
      )}
      {activeTab === "price" && (
        <div>
          <p>
            <label>from:</label>
            <input {...register(`orders.${index}.price.from`)} />
          </p>
          <p>
            <label>to:</label>
            <input {...register(`orders.${index}.price.to`)} />
          </p>
          <p>
            <label>currency:</label>
            <select {...register(`orders.${index}.price.currency`)}>
              {currenciesOptions.map((currencyOption) => {
                return (
                  <option
                    key={currencyOption.value}
                    value={currencyOption.value}
                  >
                    {currencyOption.label}
                  </option>
                );
              })}
            </select>
          </p>
        </div>
      )}
      <button
        onClick={() => {
          remove(index);
        }}
      >
        remove
      </button>
      <hr />
    </div>
  );
};

export default Order;
