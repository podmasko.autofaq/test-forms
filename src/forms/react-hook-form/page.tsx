import React from "react";
import {
  Control,
  useFieldArray,
  useForm,
  UseFormGetValues,
  UseFormRegister,
  UseFormSetValue,
} from "react-hook-form";
import {
  bmwBrand,
  bmwModels,
  brands,
  brandsWhichCanMadeInGermany,
  currencies,
  models,
} from "src/forms/react-hook-form/constants/order";
import Order, { TOrderFormValues } from "src/forms/react-hook-form/order";
import { TOrder } from "src/forms/react-hook-form/types/order";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const defaultOrder: TOrder = {
  brand: bmwBrand,
  model: bmwModels[0],
  madeInGermany: false,
  price: {
    from: 0,
    to: 0,
    currency: "usd",
  },
};

type TOrdersFormValues = {
  orders: TOrder[];
};

const schema = yup.object().shape({
  orders: yup.array().of(
    yup.object().shape({
      brand: yup
        .mixed()
        .oneOf([...brands])
        .required(),
      model: yup
        .mixed()
        .oneOf([...models])
        .required(),
      madeInGermany: yup.mixed().when("orders[].brand", {
        is: (brand: TOrder["brand"]) =>
          brandsWhichCanMadeInGermany.some(
            (brandFromGermany) => brandFromGermany === brand
          ),
        then: yup.boolean().required(),
        otherwise: yup.mixed().default(false),
      }),
      price: yup
        .object()
        .shape({
          from: yup.number().positive().required(),
          to: yup.number().positive().required(),
          currency: yup
            .mixed()
            .oneOf([...currencies])
            .required(),
        })
        .required(),
    })
  ),
});

const Page = () => {
  const { control, handleSubmit, register, resetField, setValue, getValues } =
    useForm<TOrdersFormValues>({
      defaultValues: {
        orders: [{ ...defaultOrder }],
      },
      resolver: yupResolver(schema),
    });
  const { fields, prepend, remove } = useFieldArray({
    control,
    name: "orders",
  });

  const onSave = (data: TOrdersFormValues) => {
    alert(
      `saved orders\n-\n${data.orders
        .map((order) => JSON.stringify(order))
        .join("\n")}`
    );
  };

  const onAdd = () => {
    prepend({
      ...defaultOrder,
    });
  };

  // TODO: disable

  return (
    <div>
      <div>
        <p>
          <button onClick={handleSubmit(onSave)}>save</button>
          <button onClick={onAdd}>add</button>
        </p>
      </div>
      <hr />
      <hr />
      <hr />
      <form>
        {fields.map((field, index) => {
          return (
            <Order
              key={field.id}
              register={
                register as unknown as UseFormRegister<TOrderFormValues> // TODO: убрать кастование
              }
              control={
                control as unknown as Control<TOrderFormValues> // TODO: убрать кастование
              }
              remove={remove}
              index={index}
              resetField={resetField}
              setValue={
                setValue as unknown as UseFormSetValue<TOrderFormValues>
              }
              getValues={
                getValues as unknown as UseFormGetValues<TOrderFormValues>
              }
            />
          );
        })}
      </form>
    </div>
  );
};

export default Page;
