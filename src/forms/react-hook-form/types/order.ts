import {
  audiBrand,
  audiModels,
  bmwBrand,
  bmwModels,
  brands,
  toyotaBrand,
  toyotaModels,
} from "src/forms/react-hook-form/constants/order";

export type TOrder = {
  brand: typeof brands[number];
  model:
    | typeof bmwModels[number]
    | typeof audiModels[number]
    | typeof toyotaModels[number];
  madeInGermany: boolean;
  price: {
    from: number;
    to: number;
    currency: "usd" | "eur";
  };
} & (
  | {
      brand: typeof bmwBrand;
      model: typeof bmwModels[number];
      madeInGermany: boolean;
    }
  | {
      brand: typeof audiBrand;
      model: typeof audiModels[number];
      madeInGermany: boolean;
    }
  | {
      brand: typeof toyotaBrand;
      model: typeof toyotaModels[number];
      madeInGermany: false;
    }
);
