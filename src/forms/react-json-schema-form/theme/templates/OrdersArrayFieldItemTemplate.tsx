import React from "react";
import { ArrayFieldTemplateItemType } from "@rjsf/utils";

const OrdersArrayFieldItemTemplate = ({
  children,
  disabled,
  index,
  onDropIndexClick,
  readonly,
  registry,
  uiSchema,
}: ArrayFieldTemplateItemType) => {
  const { RemoveButton } = registry.templates.ButtonTemplates;

  return (
    <>
      {children}
      <RemoveButton
        disabled={disabled || readonly}
        onClick={onDropIndexClick(index)}
        style={{ width: "100%" }}
        uiSchema={uiSchema}
      />
    </>
  );
};

export default OrdersArrayFieldItemTemplate;
