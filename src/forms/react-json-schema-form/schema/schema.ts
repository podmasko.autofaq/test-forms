import { RJSFSchema } from "@rjsf/utils";

const bmwBrand = "bmw";
const bmwModels = [
  "1series",
  "2series",
  "3series",
  "4series",
  "5series",
] as const;
const bmwModelsTitles = bmwModels.reduce<
  Record<keyof typeof bmwModels, string>
>((modelsTitles, model) => {
  modelsTitles[model as keyof typeof bmwModels] = `${model[0]} ${model.slice(
    1
  )}`;

  return modelsTitles;
}, {} as Record<keyof typeof bmwModels, string>);

const audiBrand = "audi";
const audiModels = ["a3", "a4", "a5", "a6", "a7"];
const audiModelsTitles = audiModels.reduce<
  Record<keyof typeof audiModels, string>
>((modelsTitles, model) => {
  modelsTitles[
    model as keyof typeof audiModels
  ] = `${model[0].toUpperCase()}${model.slice(1)}`;

  return modelsTitles;
}, {} as Record<keyof typeof audiModels, string>);

const toyotaBrand = "toyota";
const toyotaModels = ["gt86", "supra"];
const toyotaModelsTitles = {
  gt86: "GT86",
  supra: "Supra",
};

const brands = [bmwBrand, audiBrand, toyotaBrand] as const;

const brandsToModels = {
  [bmwBrand]: bmwModels,
  [audiBrand]: audiModels,
  [toyotaBrand]: toyotaModels,
};

const brandsToModelsTitles = {
  [bmwBrand]: bmwModelsTitles,
  [audiBrand]: audiModelsTitles,
  [toyotaBrand]: toyotaModelsTitles,
};

const brandsTitles = {
  [bmwBrand]: "BMW",
  [audiBrand]: "Audi",
  [toyotaBrand]: "Toyota",
} as const;

const brandFromGermany = [bmwBrand, audiBrand] as const;

const schema: RJSFSchema = {
  type: "object",
  properties: {
    orders: {
      type: "array",
      title: "Заказы",
      items: {
        type: "object",
        title: "Заказ",
        properties: {
          brand: {
            title: "Бренд",
            type: "string",
            oneOf: brands.map((brand) => ({
              const: brand,
              title: brandsTitles[brand],
            })),
            default: brands[0],
          },
          model: {
            type: "string",
          },
          createdAt: {
            type: "string",
            title: "Год производства",
            oneOf: [
              {
                const: "new",
                title: "Новый автомобиль (текущий год)",
              },
              {
                const: "set-year",
                title: "Выбрать год изготовления",
              },
            ],
          },
        },
        dependencies: {
          brand: {
            oneOf: brands.map((brand) => {
              const oneOfStatement = {
                properties: {
                  brand: {
                    const: brand,
                  },
                  model: {
                    type: "string" as "string",
                    title: "Модель",
                    oneOf: brandsToModels[brand].map((model) => ({
                      const: model,
                      // @ts-ignore
                      title: brandsToModelsTitles[brand][model], // TODO: фикс
                    })),
                    default: brandsToModels[brand][0],
                  },
                },
                required: ["model"],
              };

              // @ts-ignore
              if (brandFromGermany.includes(brand)) {
                // @ts-ignore
                oneOfStatement.properties.madeInGermany = {
                  type: "boolean",
                  title: "Изготовлен в Германии",
                };
              }

              return oneOfStatement;
            }),
          },
          createdAt: {
            oneOf: [
              {
                properties: {
                  createdAt: {
                    const: "new",
                  },
                },
              },
              {
                properties: {
                  createdAt: {
                    const: "set-year",
                  },
                  createdAtYear: {
                    title: "Год изготовления",
                    type: "number",
                    minimum: 1900,
                    maximum: new Date().getFullYear(),
                  },
                },
                required: ["createdAtYear"],
              },
            ],
          },
        },
        required: ["brand"],
      },
    },
  },
  required: ["orders"],
};

export default schema;
