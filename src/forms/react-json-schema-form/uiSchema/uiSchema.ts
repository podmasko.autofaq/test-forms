import { UiSchema } from "@rjsf/utils";

const uiSchema: UiSchema = {
  "ui:submitButtonOptions": {
    norender: true,
  },
  orders: {
    items: {
      "ui:order": [
        "brand",
        "model",
        "madeInGermany",
        "createdAt",
        "createdAtYear",
      ],
    },
  },
};

export default uiSchema;
