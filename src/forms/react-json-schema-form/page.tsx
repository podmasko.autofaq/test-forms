import React, { useRef } from "react";
import validator from "@rjsf/validator-ajv8";
import schema from "src/forms/react-json-schema-form/schema/schema";
import uiSchema from "src/forms/react-json-schema-form/uiSchema/uiSchema";
import Form from "@rjsf/antd";
import "antd/dist/antd.min.css";
import OrdersArrayFieldTemplate from "src/forms/react-json-schema-form/theme/templates/OrdersArrayFieldTemplate";
import OrdersArrayFieldItemTemplate from "src/forms/react-json-schema-form/theme/templates/OrdersArrayFieldItemTemplate";

const Page = () => {
  const formRef = useRef<any>();
  const addButtonRef = useRef<any>();

  const onSubmit = (data: any) => {
    alert(
      `saved orders\n-\n${data.formData.orders
        .map((order: any) => JSON.stringify(order))
        .join("\n")}`
    );
  };

  const triggerSubmit = () => {
    formRef.current.submit();
  };

  const triggerAdd = () => {
    addButtonRef.current.click();
  };

  const templates = {
    ArrayFieldTemplate: (props: any, context: any) => {
      return (
        <OrdersArrayFieldTemplate
          {...props}
          {...context}
          addButtonRef={addButtonRef}
        />
      );
    },
    ArrayFieldItemTemplate: OrdersArrayFieldItemTemplate,
  };

  return (
    <div style={{ width: "500px", padding: "50px" }}>
      <button type="button" onClick={triggerSubmit}>
        save
      </button>
      <button type="button" onClick={triggerAdd}>
        add
      </button>
      <Form
        showErrorList={false}
        schema={schema}
        uiSchema={uiSchema}
        validator={validator}
        onSubmit={onSubmit}
        templates={templates}
        // @ts-ignore
        ref={(ref: any) => (formRef.current = ref)}
      />
    </div>
  );
};

export default Page;
