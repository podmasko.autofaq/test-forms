import { JsonForms } from "@jsonforms/react";
// import { vanillaCells, vanillaRenderers } from "@jsonforms/vanilla-renderers";
import {
  materialCells,
  materialRenderers,
} from "@jsonforms/material-renderers";
import uiSchema from "src/forms/jsonforms-react/uiSchema";

import React, { useState } from "react";
import schema from "src/forms/jsonforms-react/schema";

const Page = () => {
  const [state, setState] = useState({ orders: [] });

  return (
    <JsonForms
      data={state}
      schema={schema}
      uischema={uiSchema}
      renderers={materialRenderers}
      cells={materialCells}
      onChange={({ errors, data }) => {
        console.log(data, errors);
        setState(data);
      }}
    />
  );
};

export default Page;
