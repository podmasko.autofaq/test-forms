const schema = {
  type: "object",
  properties: {
    shapeType: {
      type: "string",
      enum: ["", "square", "circle", "rectangle"],
    },
    shapeProperties: {
      properties: {
        radius: {
          type: "number",
        },
        height: {
          type: "number",
        },
        width: {
          type: "number",
        },
      },
    },
  },
  if: {
    not: {
      properties: {
        shapeType: {
          const: "",
        },
      },
    },
  },
  then: {
    required: ["shapeProperties"],
  },
  allOf: [
    {
      if: {
        properties: {
          shapeType: {
            const: "square",
          },
        },
      },
      then: {
        properties: {
          shapeProperties: {
            required: ["height"],
          },
        },
      },
    },
    {
      if: {
        properties: {
          shapeType: {
            const: "circle",
          },
        },
      },
      then: {
        properties: {
          shapeProperties: {
            required: ["radius"],
          },
        },
      },
    },
    {
      if: {
        properties: {
          shapeType: {
            const: "rectangle",
          },
        },
      },
      then: {
        properties: {
          shapeProperties: {
            required: ["height", "width"],
          },
        },
      },
    },
  ],
};

export default schema;
