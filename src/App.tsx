import React from "react";
import RHFPage from "./forms/react-hook-form/page";
import JsonformsReactPage from "./forms/jsonforms-react/page";
import RJSFPage from "./forms/react-json-schema-form/page";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/rhf" element={<RHFPage />} />
        <Route path="/jsonforms-react" element={<JsonformsReactPage />} />
        <Route path="/rjsf" element={<RJSFPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
